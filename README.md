
1. Create virtual environment (for more details: http://docs.python-guide.org/en/latest/dev/virtualenvs/)
2. Install project requirements: `pip install -r requirements.txt`
3. Set virtual environment's variable for twitter consumer and access token keys and secrets:
    `cd <your_virtualenv_dir_for_project>/bin`
     open `postactivate` file 
     put following:

        export consumer_key='consumer_key'
        export consumer_secret='consumer_secret'
        export access_token_key='access_token_key'
        export access_token_secret='access_token_secret'


 4. run `./manage.py runserver`