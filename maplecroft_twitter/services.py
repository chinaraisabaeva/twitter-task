from django.conf import settings

import twitter


api = twitter.Api(consumer_key=settings.CONSUMER_KEY,
                  consumer_secret=settings.CONSUMER_SECRET,
                  access_token_key=settings.ACCESS_TOKEN_KEY,
                  access_token_secret=settings.ACCESS_TOKEN_SECRET)


def get_latest_timeline(screen_name, limit=None):
    latest_tweets = api.GetUserTimeline(screen_name=screen_name, count=limit)
    return latest_tweets


def create_data_for_map(tweets):
    """
    Return list of coordinates for google map if tweet has location.
    We need to swipe places of coordinates for google map to show location.
    """
    data = [['Lat', 'Long', 'Name'], ]
    for tweet in tweets:
        if tweet.place:
            try:
                coordinates = tweet.place.get('bounding_box').get('coordinates')
                data_element = [coordinates[0][0][1], coordinates[0][0][0], str('Tweet from %s' % tweet.created_at)]
                data.append(data_element)
            except:
                pass
    return data
