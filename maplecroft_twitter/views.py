import logging

from django.views.generic import TemplateView
from django.shortcuts import render

from services import get_latest_timeline, create_data_for_map

logger = logging.getLogger('django')


class Home(TemplateView):
    tweets_per_page = 10

    def get(self, request, *args, **kwargs):
        context = self.get_context_data()
        tweets = []
        map_data = []
        try:
            tweets = get_latest_timeline(screen_name='MaplecroftRisk', limit=self.tweets_per_page)
            map_data = create_data_for_map(tweets)
        except Exception as e:
            logger.error(e.message)
        else:
            context.update(tweets=tweets)
            context.update(map_data=map_data)
        return render(request, template_name='index.html', context=context)
